# System of equations
# x1' = a*x1 + b*x2
# x2' = c*x1 + b*x2
# Forward Euler method
# x1(t+1) = x1(t) + dt*(a*x1(t)+b*x2(t))  
# x2(t+1) = x2(t) + dt*(c*x1(t)+d*x2(t))
# On Euler: download modules
# $ env2lmod
# $ module load gcc/6.3.0 openmpi/4.0.2 r/4.0.2
# To calculate on 4 processors
# $ export MPI_UNIVERSE_SIZE=5

# Load Rmpi which calls mpi.initialize()
library(Rmpi)

# In the environment export MPI_UNIVERSE_SIZE
# usize = Ncountries + 1
usize <- as.numeric(Sys.getenv("MPI_UNIVERSE_SIZE"))
ns <- usize - 1

# Spawn 'ns' R-slaves to the host
# ns should equal Ncountries.
mpi.spawn.Rslaves(nslaves=ns)

# Initialize state variables and parameters
Ncountries=ns
factors = list()
states_all = list()
params_all = list()
for (i in 1:Ncountries){
    factors[[i]] <- c(1.0, 1.0)
    states_all[[i]] <- c(i, i*10)
    params_all[[i]] <- c(0.1, 0.1, 0.2, 0.2)
}

print(factors)
print(states_all)
print(params_all)

# Get the rank number of that processor
mpi.bcast.cmd(id <- mpi.comm.rank())

# Root sends state variables and parameters to other ranks
mpi.scatter.Robj2slave(factors, comm = 1)
mpi.scatter.Robj2slave(states_all, comm = 1)
mpi.scatter.Robj2slave(params_all, comm = 1)

mpi.remote.exec(paste(factors))
mpi.remote.exec(paste(states_all))
mpi.remote.exec(paste(params_all))

# This function performs one time step of an ODE solver
one_time_step <- function(istep,states_one,params_one,factors){

    dt = 0.01    
    x1_t <- states_one[1]*factors[1]
    x2_t <- states_one[2]*factors[2]

    a <- params_one[1]
    b <- params_one[2]
    c <- params_one[3]
    d <- params_one[4]

    x1_t1 <- x1_t + dt*(a*x1_t+b*x2_t)
    x2_t1 <- x2_t + dt*(c*x1_t+d*x2_t)

    return(c(x1_t1, x2_t1))
}

# Root sends the function to the other ranks
mpi.bcast.Robj2slave(one_time_step)

update_state_vars <- function(states_in){
    update = list()
    for (i in 2:usize){
        irank = i-1
        update[[irank]] <- c(states_in[1,i]/sum(states_in[1,]),states_in[2,i]/sum(states_in[2,]))
    }
    return(update)
}

# Do time stepping
for (i in 1:2){

    print(paste("Time step ", i))
    # Root asks the other ranks to perform the function
    mpi.bcast.cmd(states_all <- one_time_step(id, states_all, params_all, factors))
    # mpi.remote.exec(paste(states_all))

    # Root orders other ranks to gather the output
    mpi.bcast.cmd(mpi.gather.Robj(states_all))
    # Root gathers the output from other ranks
    states_t1 <- mpi.gather.Robj(double(2))

    print(states_t1)
    
    # Root uses these output values to update the state variables 
    #states_all <- update_state_vars(states_t1)
    #print(paste("updated state variables"))
    #print(paste(states_all))
    # Root send the update factors to other ranks
    #mpi.scatter.Robj2slave(states_all, comm = 1)

    factors <- update_state_vars(states_t1)

    print(paste("update factors"))
    print(paste(factors))

    # Root send the update factors to other ranks
    mpi.scatter.Robj2slave(factors, comm = 1)
}

mpi.close.Rslaves(dellog = FALSE)
mpi.quit()
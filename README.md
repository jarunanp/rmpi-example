# Rmpi-Example

Parallelize an R code with Rmpi

Login to Euler

```
$ ssh username@euler.ethz.ch
```

Load modules

```
$ env2lmod
$ module load gcc/6.3.0 openmpi/4.0.2 r/4.0.2
```

To test, request 5 processors. MPI_UNIVERSE_SIZE is checked inside the R script for how many processors are available. One processor is to be the master and others for calculation.

```
$ export MPI_UNIVERSE_SIZE=5
$ bsub -n 5 -W 2:00 -R ib -Is bash
Generic job.
Job <155035882> is submitted to queue <normal.4h>.
<<Waiting for dispatch ...>>
<<Starting on eu-c7-105-15>>
```

Run the R script
```
Rscript test_ode_solver.R
```
